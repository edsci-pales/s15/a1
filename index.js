const fName = document.getElementById("first-name");
const lName = document.getElementById("last-name");
const correctAnswers = {
      q1: "wolf",
      q2: "whale"
    };

const submitButton = document.querySelector("#submit-button");
const totalScore = document.querySelector("#total-score");
const question1 = document.querySelector("#q1").value;
const question2 = document.querySelector("#q2").value;
const questionMessage = document.querySelector("#a1");
const questionMessage2 = document.querySelector("#a2");
let score = 0;

submitButton.addEventListener("click", () => {
    if(question1 === correctAnswers.q1){
        questionMessage.innerText = "Correct!";
        questionMessage.style.color = "green";
        score++
    }
    else{
        questionMessage.innerText = "Incorrect!";
        questionMessage.style.color = "red";
    }
    
    if(question2 === correctAnswers.q2){
        questionMessage2.innerText = "Correct!";
        questionMessage2.style.color = "green";
        score++
    }
    else{
        questionMessage2.innerText = "Incorrect!";
        questionMessage2.style.color = "red";
    }
    
});

totalScore.innerText = `Hello ${fName.value} ${lName.value},Your total score is ${score} out of 2`;

alert(totalScore.innerText)
